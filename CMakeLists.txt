cmake_minimum_required(VERSION 3.27)

project(SnowFlakeID
        VERSION 1.0
        LANGUAGES CXX)
        

# Add software components 
add_subdirectory("src" SnowFlakeID_Library)

# Add Test-module
add_subdirectory("test" SnowFlakeID_Tests)

