/*
 * Copyright (c) 2024 Erik Ridderby, ARCHEA.
 * All rights reserved.
 *
 * This source code is licensed under both the
 * * BSD-3-Clause license with No Nuclear or Weapons use exception
 *   (found in the BSD-3-Clause-No-Nuclear-or-Weapons-License.txt file in
 *   the root directory of this source tree)
 *
 * and the
 *
 * * GNU Affero General Public License
 *   (found in the agpl-3.0.txt file in the root directory of this source tree).
 *
 * You may select, at your option, one of the above-listed licenses.
 *
 */


#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include "SnowFlakeID/snowflakeid.h"

#include <QDateTime>
#include <QTimeZone>

using namespace testing;

TEST(BasicValidation, TimeStampTests)
{

    QDateTime orgTime = QDateTime::currentDateTimeUtc();

    quint64   timeCode = orgTime.toMSecsSinceEpoch();

    // Setup the result
    QTimeZone   tz(QTimeZone::UTC);
    QDateTime timeStamp = QDateTime::fromMSecsSinceEpoch(timeCode, tz);

    EXPECT_EQ(orgTime, timeStamp);


}

TEST(BasicValidation,BasicTests)
{
    quint32 context1 = SnowFlakeID::GenerateContext();

    SnowFlakeID id1;

    // qDebug() << "ID1 as string: " << id1.toString();
    EXPECT_EQ(id1.toString(), "(null) (null) (null) (null)");

    // qDebug() << "Is ID1 valid?" << id1.isValid();
    EXPECT_FALSE(id1.isValid());
    EXPECT_TRUE(id1.isNull());


    id1 = SnowFlakeID::FromContext(context1);

    // qDebug() << "Is ID1 valid?" << id1.isValid();

    EXPECT_TRUE(id1.isValid());
    EXPECT_FALSE(id1.isNull());

}


TEST(BasicValidation,HashTests)
{
    quint32 context1 = SnowFlakeID::GenerateContext();

    SnowFlakeID id1 = SnowFlakeID::FromContext(context1);

    QHash<quint128, QString>    objects;

    QString fid = id1.toString();
    // qDebug() << "Snow flake ID is: " << fid;

    objects.insert(id1.hash(), fid);

    qDebug() << "From Hash Table:" << objects.value(id1.hash());;
    EXPECT_EQ(objects.value(id1.hash()), fid);

}


TEST(BasicValidation,TestEquality)
{
    quint32 context1 = SnowFlakeID::GenerateContext();
    SnowFlakeID id1 = SnowFlakeID::FromContext(context1);
    SnowFlakeID id2(id1);
    SnowFlakeID id3;

    // qDebug() << "Snow flake ID1 is: " << id1;
    // qDebug() << "Snow flake ID2 is: " << id2;
    // qDebug() << "Snow flake ID3 is: " << id3;
    // qDebug() << "ID1 == ID2: " << (id1 == id2);
    // qDebug() << "ID1 == ID3: " << (id1 == id3);
    // qDebug() << "ID2 == ID3: " << (id2 == id3);

    EXPECT_TRUE(id1 == id2);
    EXPECT_FALSE(id1 == id3);
    EXPECT_FALSE(id2 == id3);

    id3 = id1;

    // qDebug() << "Snow flake ID1 is: " << id1;
    // qDebug() << "Snow flake ID2 is: " << id2;
    // qDebug() << "Snow flake ID3 is: " << id3;
    // qDebug() << "ID1 == ID2: " << (id1 == id2);
    // qDebug() << "ID1 == ID3: " << (id1 == id3);
    // qDebug() << "ID2 == ID3: " << (id2 == id3);

    EXPECT_TRUE(id1 == id2);
    EXPECT_TRUE(id1 == id3);
    EXPECT_TRUE(id2 == id3);


    SnowFlakeID empty1;
    SnowFlakeID empty2;
    // qDebug() << "Empty flake == empty falke? " << (empty1, empty2);
    EXPECT_TRUE(empty1 == empty2);

}


TEST(BasicValidation,testTransferUsingHashValue)
{
    quint32 context1 = SnowFlakeID::GenerateContext();
    SnowFlakeID id1 = SnowFlakeID::FromContext(context1);

    qDebug() << "Transfer using hash-value";

    qDebug() << id1.hashString();
    SnowFlakeID id4 = SnowFlakeID::FromHash(id1.hash());
    qDebug() << "Snow flake ID1 is: " << id1;
    qDebug() << "Snow flake ID4 is: " << id4;

    EXPECT_EQ(id1, id4);
    EXPECT_EQ(id1.toString(), id4.toString());
    EXPECT_EQ(id1.hash(), id4.hash());
    EXPECT_EQ(id1.hashString(), id4.hashString());

    SnowFlakeID id5 = SnowFlakeID::FromHash(0x00);
    EXPECT_FALSE(id5.isValid());
    EXPECT_TRUE(id5.isNull());
    qDebug() << "0x00 string: " << id5.toString();
    EXPECT_EQ(id5.toString(), "1970-01-01 00:00:00.000 (null) (null)");

}


TEST(BasicValidation,testTransferUsingHashString)
{
    qDebug() << "Transfer using hash-string";

    quint32 context1 = SnowFlakeID::GenerateContext();
    SnowFlakeID id1 = SnowFlakeID::FromContext(context1);
    qDebug() << "Snow flake ID1 is   : " << id1;
    qDebug() << "           ID1 hash : " << id1.hashString();

    SnowFlakeID id5 = SnowFlakeID::FromHash(id1.hashString());
    qDebug() << "Snow flake ID5 is   : " << id5;
    qDebug() << "           ID5 hash : " << id5.hashString();

    EXPECT_EQ(id1, id5);
    EXPECT_EQ(id1.toString(), id5.toString());
    EXPECT_EQ(id1.hash(), id5.hash());
    EXPECT_EQ(id1.hashString(), id5.hashString());


}


TEST(BasicValidation,testTransferUsingBrokenHashString)
{
    qDebug() << "Transfer using broken hash-string";
    // qDebug() << "OK: " << SnowFlakeID::FromHash(QStringLiteral("0000018dfe44df13:75c11230ce09d350"));
    // qDebug() << "OK: " << SnowFlakeID::FromHash(QStringLiteral("0000018dFe44df13:75c11230ce09d350"));
    // qDebug() << "OK: " << SnowFlakeID::FromHash(QStringLiteral("0000018dfe44dF13:75c11230CE09d350"));

    EXPECT_EQ(SnowFlakeID::FromHash(("0000018dfe44df13:75c11230ce09d350")).toString(),
            "2024-03-02 08:25:42.931 lilad-damub suman-tatib");

    EXPECT_EQ(SnowFlakeID::FromHash(QStringLiteral("0000018dFe44df13:75c11230ce09d350")).toString(),
            "2024-03-02 08:25:42.931 lilad-damub suman-tatib");

    EXPECT_EQ(SnowFlakeID::FromHash(QStringLiteral("0000018dfe44dF13:75c11230CE09d350")).toString(),
            "2024-03-02 08:25:42.931 lilad-damub suman-tatib");

    // qDebug() << "Null: " << SnowFlakeID::FromHash(QStringLiteral("0000018dfe44df13_75c11230ce09d350"));
    // qDebug() << "Null: " << SnowFlakeID::FromHash(QStringLiteral("0000018dfe44df13:75c11230ce09d35"));
    qDebug() << "Null: " << SnowFlakeID::FromHash(QStringLiteral("0000018dfe44df1:75c11230ce09d350"));
    // qDebug() << "Null: " << SnowFlakeID::FromHash(QStringLiteral("0000018dfe44df13:75c11230ce09d35x"));

    EXPECT_EQ(SnowFlakeID::FromHash(("0000018dfe44df13_75c11230ce09d350")).toString(), "(null) (null) (null) (null)");
    EXPECT_FALSE(SnowFlakeID::FromHash(("0000018dfe44df13_75c11230ce09d350")).isValid());
    EXPECT_TRUE(SnowFlakeID::FromHash(("0000018dfe44df13_75c11230ce09d350")).isNull());

    EXPECT_EQ(SnowFlakeID::FromHash(("0000018dfe44df13:75c11230ce09d35")).toString(), "(null) (null) (null) (null)");
    EXPECT_FALSE(SnowFlakeID::FromHash(("0000018dfe44df13:75c11230ce09d35")).isValid());
    EXPECT_TRUE(SnowFlakeID::FromHash(("0000018dfe44df13:75c11230ce09d35")).isNull());

    EXPECT_EQ(SnowFlakeID::FromHash(("0000018dfe44df1:75c11230ce09d350")).toString(), "(null) (null) (null) (null)");
    EXPECT_FALSE(SnowFlakeID::FromHash(("0000018dfe44df1:75c11230ce09d350")).isValid());
    EXPECT_TRUE(SnowFlakeID::FromHash(("0000018dfe44df1:75c11230ce09d350")).isNull());


    EXPECT_EQ(SnowFlakeID::FromHash(("0000018dfe44df13:75c11230ce09d35x")).toString(), "(null) (null) (null) (null)");
    EXPECT_FALSE(SnowFlakeID::FromHash(("0000018dfe44df13:75c11230ce09d35x")).isValid());
    EXPECT_TRUE(SnowFlakeID::FromHash(("0000018dfe44df13:75c11230ce09d35x")).isNull());
}


TEST(BasicValidation,testCreateFromString)
{
    qDebug() << "Create from string:";
    SnowFlakeID flake;

    // qDebug() << "Ok: " << SnowFlakeID::FromString("2024-03-02 08:25:42.931 lilad-damub suman-tatib");
    // qDebug() << "Ok: " << SnowFlakeID::FromString("2024-03-02 08:25:42.931 lilad-damub SUMAN-TATIB");

    flake = SnowFlakeID::FromString("2024-03-02 08:25:42.931 lilad-damub suman-tatib");
    EXPECT_TRUE(flake.isValid() );
    EXPECT_FALSE(flake.isNull() );
    EXPECT_EQ(flake.toString(), "2024-03-02 08:25:42.931 lilad-damub suman-tatib");

    flake = SnowFlakeID::FromString("2024-03-02 08:25:42.931 lilad-damub SUMAN-TATIB");
    EXPECT_TRUE(flake.isValid() );
    EXPECT_FALSE(flake.isNull() );
    EXPECT_EQ(flake.toString(), "2024-03-02 08:25:42.931 lilad-damub suman-tatib");

    // flake = SnowFlakeID::FromString("2024-03-02 08:25:42.931 lilad-damub babab-babab");
    // qDebug() << "Invalid: " << flake << ", isValid: " << flake.isValid();

    // Context or Random can not be 0x00, that means null.

    flake = SnowFlakeID::FromString("2024-03-02 08:25:42.931 lilad-damub babab-babab");
    EXPECT_FALSE(flake.isValid() );
    EXPECT_TRUE(flake.isNull() );
    EXPECT_EQ(flake.toString(), "2024-03-02 08:25:42.931 lilad-damub (null)");


    // flake = SnowFlakeID::FromString("2024-03-02 08:25:42.931 babab-babab suman-tatib");
    // qDebug() << "OK: " << flake << ", isValid: " << flake.isValid();

    flake = SnowFlakeID::FromString("2024-03-02 08:25:42.931 babab-babab suman-tatib");
    EXPECT_FALSE(flake.isValid() );
    EXPECT_TRUE(flake.isNull() );
    EXPECT_EQ(flake.toString(), "2024-03-02 08:25:42.931 (null) suman-tatib");

    // flake = SnowFlakeID::FromString("babab-babab babab-babab babab-babab suman-tatib");
    // qDebug() << "Invalid: " << flake << ", isValid: " << flake.isValid();

    flake = SnowFlakeID::FromString("babab-babab babab-babab babab-babab suman-tatib");
    EXPECT_FALSE(flake.isValid() );
    EXPECT_TRUE(flake.isNull() );
    EXPECT_EQ(flake.toString(), "(null) (null) (null) suman-tatib");

    // flake = SnowFlakeID::FromString("suman-tatib babab-babab babab-babab suman-tatib");
    // qDebug() << "Invalid: " << flake << ", isValid: " << flake.isValid();


    flake = SnowFlakeID::FromString("suman-tatib babab-babab babab-babab suman-tatib");
    EXPECT_FALSE(flake.isValid() );
    EXPECT_TRUE(flake.isNull() );
    EXPECT_EQ(flake.toString(), "(null) (null) (null) suman-tatib");

}


TEST(BasicValidation,testCreateFromStringWithNullFields)
{
    qDebug() << "Create from string:";
    SnowFlakeID flake;

    qDebug() << "From string with null-fields";

    flake = SnowFlakeID::FromString("(null) 08:25:42.931 lilad-damub suman-tatib");
    // qDebug() << "Invalid: " << flake << ", isValid: " << flake.isValid();
    EXPECT_FALSE(flake.isValid() );
    EXPECT_TRUE(flake.isNull() );
    EXPECT_EQ(flake.toString(), "(null) 08:25:42.931 lilad-damub suman-tatib");

    flake = SnowFlakeID::FromString("2024-03-02 (null) lilad-damub suman-tatib");
    // qDebug() << "Invalid: " << flake << ", isValid: " << flake.isValid();
    EXPECT_FALSE(flake.isValid() );
    EXPECT_TRUE(flake.isNull() );
    EXPECT_EQ(flake.toString(), "2024-03-02 (null) lilad-damub suman-tatib");

    flake = SnowFlakeID::FromString("2024-03-02 08:25:42.931 (null) suman-tatib");
    // qDebug() << "Invalid: " << flake << ", isValid: " << flake.isValid();
    EXPECT_FALSE(flake.isValid() );
    EXPECT_TRUE(flake.isNull() );
    EXPECT_EQ(flake.toString(), "2024-03-02 08:25:42.931 (null) suman-tatib");

    flake = SnowFlakeID::FromString("2024-03-02 08:25:42.931 lilad-damub (null)");
    // qDebug() << "Invalid: " << flake << ", isValid: " << flake.isValid();
    EXPECT_FALSE(flake.isValid() );
    EXPECT_TRUE(flake.isNull() );
    EXPECT_EQ(flake.toString(), "2024-03-02 08:25:42.931 lilad-damub (null)");

}


TEST(BasicValidation,testVariantMap)
{
    qDebug() << "VariantMaps: ";
    SnowFlakeID id7;

    // qDebug() << "Empty flake: " << id7.toVariant();

    id7 = SnowFlakeID::FromString("2024-03-02 08:25:42.931 lilad-damub suman-tatib");
    // qDebug() << "OK flake: " << id7.toVariant();
    // qDebug() << "Same as source? " << (SnowFlakeID::FromVariantMap(id7.toVariant()) == id7) << ", result: " << SnowFlakeID::FromVariantMap(id7.toVariant()).toString();

    EXPECT_TRUE(id7.isValid());
    EXPECT_EQ(SnowFlakeID::FromVariantMap(id7.toVariant()), id7);

    id7 = SnowFlakeID::FromString("2024-03-02 08:25:42.931 lilad-damub SUMAN-TATIB");
    // qDebug() << "OK flake: " << id7.toVariant();
    // qDebug() << "Same as source? " << (SnowFlakeID::FromVariantMap(id7.toVariant()) == id7) << ", result: " << SnowFlakeID::FromVariantMap(id7.toVariant()).toString();

    EXPECT_TRUE(id7.isValid());
    EXPECT_EQ(SnowFlakeID::FromVariantMap(id7.toVariant()), id7);

    id7 = SnowFlakeID::FromString("suman-tatib babab-babab babab-babab suman-tatib");
    // qDebug() << "NULL flake: " << id7.toVariant();
    // qDebug() << "Same as source? " << (SnowFlakeID::FromVariantMap(id7.toVariant()) == id7) << ", result: " << SnowFlakeID::FromVariantMap(id7.toVariant()).toString();

    EXPECT_TRUE(id7.isNull());
    EXPECT_EQ(SnowFlakeID::FromVariantMap(id7.toVariant()), id7);

}
