# SnowFlakeID

Qt and C++ based implementation of an 128 bit generic SnowFlakeID. 

## Rather unique Identifiers
This implementation uses 128 bits to build an ID where 64 bits are a timecode (UTC in milliseconds since EPOC), 
32 bits indicates an context and the last 32 bits is a random value. 

This is similar to some UUID version and has the same length as UUID. 

## Quite human readable
Thing is with UUID that it is heart near impossible for a human to read and remember. It is a way to 
long series of hexadecimal numbers without any obvious semantics. 

Example UUID: `{a1431b04-052b-42bd-9c67-336cede672ca}`

The SnowFlakeID provide an alternative path. 

While it is possible to generate a 128bit hex number hash out of the SnowFlakeID for technical uses, 
the intention is to use a "quite human readable format" when stored in files and presented. 

Presenting the SnowFlakeID as a hash string looks even worse that UUID: `0000018e03bb2455:df3cb0fc24b8988a` 
but suits well for technical purposes and is intended as a technical HASH number. 

What make SnowFlakeID special is that it providemeans to present the SnowFlakeID as a string representation. 
The same SnowFlakeID as above will then be presented as:

`2024-03-03 09:52:59.989 tusus-ragus fifum-nofap`. 

This is a bit longer than UUID but the date and time part, which is half of the information, has clear 
semantics and can be human understood.

The second part with the context and random numbers are converted to text using QProQuint and can be 
read for a huge part of the global population.

This makes it possible for a human to read and compare two SnowFlakeID's quite easy. Do they have the same words 
at the end? If so, do they have the same date and time? And it works the other way around; Unless they have the same
date or time they are not the same ID. 

It is also possible to express the SnowFlakeID as a QVariantHash with 4 values. The example above would result in
the following:

* date = "2024-03-03" (QDate)
* time = "09:52:59.989" (QTime)
* context = "tusus-ragus" (QString)
* random = "fifum-nofap" (QString)

which is rather more simple than the UUID-string. Having the information is a QVariantMap makes it 
easy to convert to JSON. 

# Dependencies
SnowFlakeID depends on Qt Core and the [QProQuint library](https://gitlab.com/archea-foss/qt-components/snowflakeid). 

The library is developed and tested for Qt 6.4 and the use of Qy is rather fundamental.

## Unit tests
There is also an unit test application [SnowFlakeId_Tests](https://gitlab.com/archea-foss/qt-components/snowflakeid_tests) 
that can be used to validate and verify the library. 


# Copyright and License
Copyright (c) 2024 Erik Ridderby, ARCHEA.
All rights reserved.

This source code is licensed under both the 
* BSD-3-Clause license with No Nuclear or Weapons use exception (found in the BSD-3-Clause-No-Nuclear-or-Weapons-License.txt file in the root directory of this source tree) and the 
* GPLv3 (found in the COPYING file in the root directory of this source tree).

You may select, at your option, one of the above-listed licenses.

# Attribution
The idea and design of proquints are work of Daniel S. Wilkerson. The original article can be found at https://arxiv.org/html/0901.4016. 

