##########################################################################
#
# Copyright (c) 2024 Erik Ridderby, ARCHEA.
# All rights reserved.
#
# This source code is licensed under both the
# * BSD-3-Clause license with No Nuclear or Weapons use exception
#   (found in the BSD-3-Clause-No-Nuclear-or-Weapons-License.txt file in
#    the root directory of this source tree)
#
# and the
#
# * GNU Affero General Public License
#   (found in the agpl-3.0.txt file in the root directory of this source tree).
#
# You may select, at your option, one of the above-listed licenses.
#
#
##########################################################################

cmake_minimum_required(VERSION 3.27)

project(SnowFlakeID LANGUAGES CXX)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(QT NAMES Qt6 REQUIRED COMPONENTS Core)
find_package(Qt${QT_VERSION_MAJOR} REQUIRED COMPONENTS Core)

# Add imports to the library path
# list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/../share/lib)
find_library(QProQuint QProQuint HINTS "${CMAKE_CURRENT_SOURCE_DIR}/../share/lib" REQUIRED)

set(PUBLIC_INCLUDES
    SnowFlakeID_global.h
    snowflakeid.h
)

add_library(SnowFlakeID SHARED
    snowflakeid.cpp
    ${PUBLIC_INCLUDES}
)

target_include_directories(SnowFlakeID INTERFACE ${CMAKE_CURRENT_SOURCE_DIR}
                                       PUBLIC "../share/include")

target_link_libraries(SnowFlakeID   PRIVATE Qt${QT_VERSION_MAJOR}::Core)
                                    
                                        
target_compile_definitions(SnowFlakeID PRIVATE QPROQUINT_LIBRARY)

# Installs
install(TARGETS SnowFlakeID DESTINATION lib)
install(FILES ${PUBLIC_INCLUDES} DESTINATION include/SnowFlakeID)
