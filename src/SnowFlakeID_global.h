/*
 * Copyright (c) 2024 Erik Ridderby, ARCHEA.
 * All rights reserved.
 *
 * This source code is licensed under both the 
 * * BSD-3-Clause license with No Nuclear or Weapons use exception
 *   (found in the BSD-3-Clause-No-Nuclear-or-Weapons-License.txt file in 
 *   the root directory of this source tree) 
 *
 * and the 
 * 
 * * GPLv3 
 *   (found in the COPYING file in the root directory of this source tree).
 * 
 * You may select, at your option, one of the above-listed licenses.
 * 
 */
 

#ifndef SNOWFLAKEID_GLOBAL_H
#define SNOWFLAKEID_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(SNOWFLAKEID_LIBRARY)
#  define SNOWFLAKEID_EXPORT Q_DECL_EXPORT
#else
#  define SNOWFLAKEID_EXPORT Q_DECL_IMPORT
#endif

#endif // SNOWFLAKEID_GLOBAL_H
