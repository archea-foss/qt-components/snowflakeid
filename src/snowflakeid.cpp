/*
 * Copyright (c) 2024 Erik Ridderby, ARCHEA.
 * All rights reserved.
 *
 * This source code is licensed under both the 
 * * BSD-3-Clause license with No Nuclear or Weapons use exception
 *   (found in the BSD-3-Clause-No-Nuclear-or-Weapons-License.txt file in 
 *   the root directory of this source tree) 
 *
 * and the 
 * 
 * * GPLv3 
 *   (found in the COPYING file in the root directory of this source tree).
 * 
 * You may select, at your option, one of the above-listed licenses.
 * 
 */
 


#include "snowflakeid.h"

#include <QProQuint/qproquint.h>

#include <QtTypes>
#include <QRandomGenerator>
#include <QTimeZone>
#include <QVariant>


const QString sDate         = QStringLiteral("date");
const QString sTime         = QStringLiteral("time");
const QString sContext      = QStringLiteral("context");
const QString sRandom       = QStringLiteral("random");

union HashParts
{
        quint128        hash;
        struct
        {
                quint32 random:32;
                quint32 context:32;
                quint64 timestamp:64;

        } parts;

};

union HashWords
{
        quint128        hash;
        struct
        {
                quint64 lsb:64;
                quint64 msb:64;

        } words;

};


SnowFlakeID::SnowFlakeID()
    : mContext(0)
    , mRandom(0)
{
    // NULL SnowflakeId.
}

SnowFlakeID::SnowFlakeID(const SnowFlakeID &other)
    // : mTimeStamp(other.mTimeStamp)
    : mDate(other.mDate)
    , mTime(other.mTime)
    , mContext(other.mContext)
    , mRandom(other.mRandom)
{

}

SnowFlakeID::~SnowFlakeID()
{
}

SnowFlakeID SnowFlakeID::FromContext(quint32 context)
{
    SnowFlakeID result;

    if (0x0 != context)
    {
        QDateTime timeStamp = QDateTime::currentDateTimeUtc();
        result.mDate = timeStamp.date();
        result.mTime = timeStamp.time();
        result.mContext = context;
        result.mRandom = QRandomGenerator::global()->generate();
    }

    return result;

}

SnowFlakeID SnowFlakeID::FromHash(quint128 hash)
{
    SnowFlakeID result;

    HashParts convert;
    convert.hash = hash;


    QTimeZone   tz(QTimeZone::UTC);
    QDateTime timeStamp = QDateTime::fromMSecsSinceEpoch(convert.parts.timestamp, tz);
    result.mDate = timeStamp.date();
    result.mTime = timeStamp.time();

    result.mContext = convert.parts.context;    // static_cast<quint32>(lsq >> 32);
    result.mRandom  = convert.parts.random;     // static_cast<quint32>(lsq);

    return result;
}

SnowFlakeID SnowFlakeID::FromHash(QString hash)
{
    SnowFlakeID result;

    // Validate the string

    if (hash.length() != 33 || hash[16] != ':')
        return result;

    QString msqStr = hash.left(16);
    QString lsqStr = hash.right(16);

    // Convert from string
    bool bOk = true;

    quint64 timeCode = msqStr.toULongLong(&bOk, 16);
    if (!bOk)
        return result;

    quint32 context = lsqStr.left(8).toULong(&bOk, 16);
    if (!bOk)
        return result;

    quint32 random =  lsqStr.right(8).toULong(&bOk, 16);
    if (!bOk)
        return result;

    // Setup the result
    QTimeZone   tz(QTimeZone::UTC);
    QDateTime timeStamp = QDateTime::fromMSecsSinceEpoch(timeCode, tz);

    result.mDate = timeStamp.date();
    result.mTime = timeStamp.time();

    result.mContext = context;
    result.mRandom  = random;

    return result;

}

SnowFlakeID SnowFlakeID::FromString(QString str)
{
    SnowFlakeID result;
    QStringList parts = str.split(' ');

    if (parts.size() != 4)
        return result;

    QDate   date = QDate::fromString(parts[0], Qt::ISODate);
    QTime   time = QTime::fromString(parts[1], Qt::ISODateWithMs);

    quint32 context = QProQuint::uint32FromString(parts[2].toLower().toUtf8().data());
    quint32 random  = QProQuint::uint32FromString(parts[3].toLower().toUtf8().data());

    // qDebug() << str << "Parses into: "
    //          << date
    //          << time
    //          << context
    //          << random;

    result.mDate      = date;
    result.mTime      = time;
    result.mContext   = context;
    result.mRandom    = random;

    return result;

}

SnowFlakeID SnowFlakeID::FromVariantMap(QVariantMap map)
{
    SnowFlakeID result;

    if (map.contains(sDate) &&
            map.contains(sTime) &&
            map.contains(sContext) &&
            map.contains(sRandom))
    {
        result.mDate        = map.value(sDate).toDate();
        result.mTime        = map.value(sTime).toTime();
        result.mContext     = map.value(sContext).toUInt();
        result.mRandom      = map.value(sRandom).toUInt();
    }

    return result;


}

quint32 SnowFlakeID::GenerateContext()
{
    return QRandomGenerator::global()->generate();
}



SnowFlakeID &SnowFlakeID::operator =(const SnowFlakeID &other)
{
    mDate       = other.mDate;
    mTime       = other.mTime;
    mContext    = other.mContext;
    mRandom     = other.mRandom;

    return *this;
}

bool SnowFlakeID::operator ==(const SnowFlakeID &other) const
{
    return (    other.mDate == mDate &&
                other.mTime== mTime &&
                other.mContext == mContext &&
                other.mRandom == mRandom);

}

QString SnowFlakeID::toString() const
{
    QString     result;
    QByteArray  cBuff(11, 0);

    if (mDate.isValid())
        result += mDate.toString("yyyy-MM-dd");
    else
        result += "(null)";

    if (mTime.isValid())
        result += " " + mTime.toString("HH:mm:ss.zzz");
    else
        result += " (null)";


    if (0x00 != mContext)
    {
        result += " " + QProQuint::toString(mContext, '-');
        // QProQuint::uint2quint(cBuff.data(), mContext, '-');
        // result += " " + QString::fromUtf8(cBuff).trimmed().remove('\x00');
    }
    else
        result += " (null)";

    if (0x00 != mRandom)
    {
        result += " " + QProQuint::toString(mRandom, '-');
        // QProQuint::uint2quint(cBuff.data(), mRandom, '-');
        // result += " " + QString::fromLatin1(cBuff);
    }
    else
        result += " (null)";


    return result;

}

quint128 SnowFlakeID::hash() const
{
    HashParts convert;

    QTimeZone   tz(QTimeZone::UTC);
    convert.parts.timestamp = QDateTime(mDate, mTime, tz).toMSecsSinceEpoch();
    convert.parts.context   = mContext;
    convert.parts.random    = mRandom;

    return convert.hash;

}

QString SnowFlakeID::hashString() const
{
    quint128 hashValue = hash();

    HashWords convert;

    convert.hash = hashValue;

    QString msq = QStringLiteral("0000000000000000") + QString::number(convert.words.msb, 16);
    QString lsq = QStringLiteral("0000000000000000") + QString::number(convert.words.lsb, 16);

    return msq.right(16) + ":" + lsq.right(16);
}

bool SnowFlakeID::isValid() const
{
    if (mDate.isValid() && mTime.isValid() && 0x00 != mContext && 0x00 != mRandom)
        return true;

    return false;

}

bool SnowFlakeID::isNull() const
{
    return !isValid();

}

QVariantMap SnowFlakeID::toVariant() const
{
    QVariantMap result;

    result.insert(sDate, mDate);
    result.insert(sTime, mTime);
    result.insert(sContext, mContext);
    result.insert(sRandom, mRandom);

    return result;


}


QDebug operator << (QDebug d, const SnowFlakeID& flake)
{
    QDebug dd = d.nospace().noquote();
    dd << flake.hashString() << " (" << flake.toString() << ")";

    return d;

}

